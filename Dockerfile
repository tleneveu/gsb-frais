FROM php:8.1.13-cli

WORKDIR /var/www

RUN pecl install xdebug-3.1.6 && docker-php-ext-enable xdebug

RUN docker-php-ext-install pdo pdo_mysql

CMD ["php", "-S", "0.0.0.0:8080", "-t", "public/"]

EXPOSE 8080
